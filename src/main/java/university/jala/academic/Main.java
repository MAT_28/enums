package university.jala.academic;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Deck deck = new Deck();
        Card cardPlayer = randomCards(deck);
        Card cardComputer = randomCards(deck);

        System.out.println("Player: " + cardPlayer.getCard());
        System.out.println("Computer: " + cardComputer.getCard());
        System.out.println(game(cardComputer, cardPlayer));
    }

    public static Card randomCards(Deck deck){
        Random random = new Random();
        int number = random.nextInt(12);
        return deck.getDeck().get(number);
    }
    
    public static String game(Card cardComputer, Card cardPlayer){
        if ( cardComputer.getValue() > cardPlayer.getValue()) {
            return "Perdiste";
        } else if ( cardComputer.getValue() < cardPlayer.getValue()) {
            return "Ganaste";
        }else
            return "Empate";
    }
}