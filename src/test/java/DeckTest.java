import org.junit.Test;
import university.jala.academic.Deck;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class DeckTest {
    @Test
    public void deckInitializationandShuffleTest() {
        Deck deck = new Deck();
        Deck deck1 = new Deck();

        int expectedSize = 52;
        int actualSize = deck.getDeck().size();

        assertEquals(expectedSize,actualSize);

        assertNotEquals(deck.getDeck(), deck1.getDeck());
    }
}
