# Enums

Crear un enum llamado "Suit" the representara los simbolos de un mazo de cartas por ejemplo: TREBOL, DIAMANTES, CORAZONES, ESPADAS. Cada manojo tendria un simbolo asociado (T, D, C, E).

Crear una clase "Card" que representara a una carta, con atributos para su palo de tipo Suit y el valor asociado (2,3,4,5,6,7,8,9,10,J,Q,K,A).

Crear una clase "Deck" donde se implementara una lista de cartas, el constructor creara la lista de cartas ordenadas aleatoriamente.

En la clase main crear un mini juego, en donde la maquina tomara una carta al azar del Deck y nos otorgara a nosotros una carta, se comparan ambas cartas y el ganador es el que tiene la carta con mas valor, considerar tambien el empate.

## Output

| Ejemplo de ejecución        | Resultado |
|-----------------------------|-----------|
| Computer: TA<br/>Player: D2 | Perdiste  |
| Computer: E9<br/>Player: DA | Ganaste   |
| Computer: TA<br/>Player: EA | Empate    |

## Diagram
![Diagrama.png](Diagrama.png)

## Test Cases

### Win
![CasoDePrueba1.png](CasoDePrueba1.png)

### Lose
![CasoDePrueba2.png](CasoDePrueba2.png)

### Draw
![CasoDePrueba3.png](CasoDePrueba3.png)

## Test Deck
![TestDeck.png](TestDeck.png)
